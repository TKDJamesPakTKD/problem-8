import java.util.Scanner;

/**
 Program to read a line of text as input
 and then display the line with the first word
 moved to the end of the line.
 Author: James Pak
 Professor Kanchanawanchai
 Programming Assignment 1
 CSC130-040N
 Last changed: May 22, 2014
 */

public class Problem8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter a line of text. No punctuation please.");

		String firstword, secondphrase;
		firstword = keyboard.next();
		secondphrase = keyboard.nextLine();
		
		System.out.println("I have rephrased that line to read:");
		System.out.println(secondphrase + " " + firstword);
	}

}
